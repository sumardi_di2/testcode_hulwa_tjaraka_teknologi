<?php
class News_model extends CI_Model
{
  public function getUser($username, $password)
  {
    $results = array();
    $this->db->select('*');
    $this->db->from('user');
    $this->db->where('username', $username);
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      $results = $query->result_array();
    }
    return $results;
  }
}
?>
