<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
  }

	public function index()
	{
		$this->load->view('order');
	}

  public function listOrder()
  {
  	$this->load->view('listorder');
  }

  public function updateOrder($id)
  {
    $results = array();
    $this->db->select('*');
    $this->db->from('foodorder');
    $this->db->join('food', 'food.id = foodorder.foodId');
    $this->db->where('orderId ', $id);
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      $results = $query->result_array();
    }
    $data = array(
    'id' => $id,
    'nomorMeja' => $results[0]['tableNumber']
    );
  	$this->load->view('updateorder', $data);
  }

  public function food()
  {
    $results = array();
    $this->db->select('*');
    $this->db->from('food');
    $this->db->where('statusFood', "Ready");
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      $results = $query->result_array();
      $this->output
        ->set_status_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($results, JSON_PRETTY_PRINT))
        ->_display();
        exit;
    }
    else{
      $this->output
        ->set_status_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($results, JSON_PRETTY_PRINT))
        ->_display();
        exit;
    }
  }

  public function order()
  {
    $results = array();
    $this->db->select('*');
    $this->db->from('foodorder');
    $this->db->join('food', 'food.id = foodorder.foodId');
    $this->db->where('statusOrder', "Active");
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      $results = $query->result_array();
      $this->output
        ->set_status_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($results, JSON_PRETTY_PRINT))
        ->_display();
        exit;
    }
    else{
      $this->output
        ->set_status_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($results, JSON_PRETTY_PRINT))
        ->_display();
        exit;
    }
  }

  public function addOrder()
  {
    $data = (array)json_decode(file_get_contents('php://input'));
    $this->db->insert('foodorder', $data);

    $response = array(
      'Success' => true,
      'Info' => 'Data Tersimpan');

    $this->output
      ->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT))
      ->_display();
      exit;
  }

  public function updateOrder1($id)
  {
    $data = (array)json_decode(file_get_contents('php://input'));
    $this->db->where('orderId', $id);
    $this->db->update('foodorder', $data);

    $response = array(
      'Success' => true,
      'Info' => 'Data Berhasil di update');

    $this->output
      ->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT))
      ->_display();
      exit;
  }
}
