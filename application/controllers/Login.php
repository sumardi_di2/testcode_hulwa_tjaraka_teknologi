<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('news_model');
  }

	public function index()
	{
		$this->load->view('login');
	}

  public function loginuser()
  {
    $data = (array)json_decode(file_get_contents('php://input'));
    $results = array();
    $this->db->select('*');
    $this->db->from('user');
    $this->db->where('username', $data['username']);
    $this->db->where('password', $data['password']);
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      $results = $query->result_array();
      $this->output
        ->set_status_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($results, JSON_PRETTY_PRINT))
        ->_display();
        exit;
    }
    else{
      $this->output
        ->set_status_header(204)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($results, JSON_PRETTY_PRINT))
        ->_display();
        exit;
    }
  }
}
