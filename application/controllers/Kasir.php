<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kasir extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
  }

	public function index()
	{
		$this->load->view('orderkasir');
	}

  public function listOrder()
  {
  	$this->load->view('listordercashier');
  }

  public function updateOrder($id)
  {
    $results = array();
    $this->db->select('*');
    $this->db->from('foodorder');
    $this->db->join('food', 'food.id = foodorder.foodId');
    $this->db->where('orderId ', $id);
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      $results = $query->result_array();
    }
    $data = array(
    'id' => $id,
    'nomorMeja' => $results[0]['tableNumber']
    );
  	$this->load->view('updateorderkasir', $data);
  }

  public function payOrder($id)
  {
    $this->db->where('orderId', $id);
    $this->db->update('foodorder', array(
      'statusOrder' => "Pay"
    ));
    redirect('kasir/listOrder');
  }
}
