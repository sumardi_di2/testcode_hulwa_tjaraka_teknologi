<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Login</title>

  <!-- Custom fonts for this template-->
  <!-- <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css"> -->
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?php echo base_url("assets/css/sb-admin-2.min.css"); ?>" rel="stylesheet">

</head>

<body class="bg-gradient-primary">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <!-- <div class="col-lg-6 d-none d-lg-block bg-login-image"></div> -->
              <div class="col-lg-12">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">LOGIN</h1>
                  </div>
                  <form class="user" method="post">
                    <div class="form-group">
                      <input type="text" class="form-control form-control-user" id="exampleInputEmail" placeholder="USERNAME">
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control form-control-user" id="exampleInputPassword" placeholder="PASSWORD">
                    </div>
                    <div class="form-group" style="color: red" id="wornguser">
                      <!-- Wrong username or password -->
                    </div>
                    <input type="submit" class="btn btn-primary btn-user btn-block" value="Login">
                    <!-- <a href="#" class="btn btn-primary btn-user btn-block" id="buttonlogin">
                      Login
                    </a> -->
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <!-- <script src="vendor/jquery/jquery.min.js"></script> -->
  <!-- <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script> -->

  <!-- Core plugin JavaScript-->
  <!-- <script src="vendor/jquery-easing/jquery.easing.min.js"></script> -->
  <script src="<?php echo base_url("assets/js/jquery-3.5.1.js"); ?>"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo base_url("assets/js/sb-admin-2.min.js"); ?>"></script>
  <script>var baseURL = "<?php echo base_url();?>"</script>
  <script>
  $(".user").submit(function(event){
  	event.preventDefault(); //prevent default action
  	// var post_url = $(this).attr("action"); //get form action url
  	// var request_method = $(this).attr("method"); //get form GET/POST method
  	// var form_data = $(this).serialize(); //Encode form elements for submission
    var form_data = {
      username: $('#exampleInputEmail').val(),
      password: $('#exampleInputPassword').val()
    };

  	$.ajax({
  		url : baseURL + "login/loginuser",
  		type: "POST",
      contentType: "application/json",
      dataType: 'json',
  		data : JSON.stringify(form_data),
      error: function(e) {
        console.log("Gagal");
      },
      success: function (data, status, headers, config) {
        if(status == "nocontent")
        {
          $("#wornguser").html("Wrong username or password");
        }
        else {
          $("#wornguser").html("");
          $.each(data, function(index, element) {
            if(element.status == "waiter")
            {
              window.location.href = baseURL + "order";
            }
            else if (element.status == "cashier") {
              window.location.href = baseURL + "kasir";
            }
          });

          // window.location.href = baseURL + "order";
        }
      }
  	})
  });
  </script>

</body>

</html>
